import java.io.*;
class Pr1{

	public static void main(String [] args)throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number of rows");
		int r = Integer.parseInt(br.readLine());
		int num=r;
		for(int i=1;i<=r;i++){
		
			for(int j=1;j<=i;j++){
			
				System.out.print(num * num);
			}
			System.out.println();
		}
	
	}
}
