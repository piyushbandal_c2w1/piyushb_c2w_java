class Program9{
public static void main (String [] args){

	int DSA=40;
	int M3=60;
	int TOC=45;
	int PPL=55;
	int MP=45;


	if(DSA>=40 && M3>=40 && TOC>=40 && PPL>=40 && MP>=40){
		int average = (DSA+M3+TOC+PPL+MP)/5;
		char grade;

		if(average>=75){
			grade='O';
		}else if(average>=65){
			grade = 'A';
		}else if(average>=55){
			grade = 'B';
		}else 
			grade = 'P';

		switch(grade){
		
			case 'O':
				System.out.println("First Class With Distinction");
				break;

			case 'A':
				System.out.println("First class");
				break;

			case 'B':
				System.out.println("Second Class");
				break;

			case 'P':
				System.out.println("Pass");
				break;
		}
	}else
		System.out.println("You Failed the course");
}

}

