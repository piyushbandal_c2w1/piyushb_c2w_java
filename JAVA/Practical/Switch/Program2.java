class Program2{
public static void main(String args[]){

	char ch='A';

	switch(ch){
		case 'O':
			System.out.println("Outstanding");
			break;

		case 'A':
			System.out.println("Excellent");
			break;

		case 'B':
			System.out.println("Very Good");
			break;
		
		case 'C':
			System.out.println("Good");
			break;

		case 'E':
			System.out.println("Just Pass");
			break;

		case 'F':
			System.out.println("Fail");
			break;

		default:
			System.out.println("Put Char btw O,A,B,C,F,E");
	
	}
}
}

