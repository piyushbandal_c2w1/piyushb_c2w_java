import java.util.Scanner;
class Pro1{

	public static void main(String [] args){
	
		Scanner sc= new Scanner(System.in);

		System.out.println("Enter the number of Rows");
		int row=sc.nextInt();

		for(int i=1; i<=row; i++){
		
			for(int j=row;j>=i;j--){
			
				System.out.print(" ");
			}
			for(int k=1;k<=i;k++){
			
				System.out.print(k);
			}
			System.out.println();
		}
		
	}
}
