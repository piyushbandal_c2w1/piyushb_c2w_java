import java.util.Scanner;

class Pat1{

	public static void main(String [] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter The Row = ");
		int r=sc.nextInt();
		
		for(int i=1;i<=r;i++){
			int num=i;
			for(int j=r;j>=i;j--){
			
				System.out.print(num++);
			}
			System.out.println();
		}

	}
}
